#!/usr/bin/env bash
# TODO: check for staging that want to build/run docker containers
helm repo add jenkins https://charts.jenkins.io
helm upgrade --install --create-namespace -n jenkins jenkins jenkins/jenkins --set controller.servicePort=80 --set controller.serviceType=LoadBalancer -f jenkins.yaml
kubectl -n jenkins wait --for=condition=ready pod -l app.kubernetes.io/name=jenkins --timeout=240s
kubectl -n jenkins get secret jenkins -o jsonpath="{.data.jenkins-admin-password}" | base64 -d; echo  # Jenkins admin password
