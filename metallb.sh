#!/usr/bin/env bash
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml
kubectl wait -n metallb-system --for=condition=ready pod -l app=metallb --timeout=90s
ipam_subnet_first_two_octets=$(docker network inspect kind | jq '.[].IPAM.Config[].Subnet' | grep -Pom 1 '\d+\.\d+' | head -n 1)  # example: 172.18
sed -E "s/(- ?)[0-9]+.[0-9]+/\1${ipam_subnet_first_two_octets}/g" metallb.yaml | kubectl apply -f -
