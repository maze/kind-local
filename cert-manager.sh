#!/usr/bin/env bash
latest_cert_manager_tag=$(curl -s https://api.github.com/repos/cert-manager/cert-manager/releases/latest | jq .tag_name -r)
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/${latest_cert_manager_tag}/cert-manager.crds.yaml
helm repo add jetstack https://charts.jetstack.io
helm upgrade --install --create-namespace -n cert-manager cert-manager jetstack/cert-manager #--set prometheus.servicemonitor.enabled=true
# TODO: check if even relevant for a kind cluster
kubectl apply -f - <<-EOF
  apiVersion: cert-manager.io/v1
  kind: ClusterIssuer
  metadata:
    name: selfsigned
  spec:
    selfSigned: {}
---
  apiVersion: cert-manager.io/v1
  kind: ClusterIssuer
  metadata:
    name: letsencrypt-staging
  spec:
    acme:
      server: https://acme-staging-v02.api.letsencrypt.org/directory
      email: maze@omg.lol
      privateKeySecretRef:
        name: letsencrypt-staging
      solvers:
      - http01:
          ingress:
            ingressClassName: nginx
---
  apiVersion: cert-manager.io/v1
  kind: ClusterIssuer
  metadata:
    name: letsencrypt-prod
  spec:
    acme:
      server: https://acme-v02.api.letsencrypt.org/directory
      email: maze@omg.lol
      privateKeySecretRef:
        name: letsencrypt-prod
      solvers:
      - http01:
          ingress:
            ingressClassName: nginx
EOF
