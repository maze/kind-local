#!/usr/bin/env bash
helm repo add grafana https://grafana.github.io/helm-charts
helm upgrade --install --create-namespace -n logging loki     grafana/loki-distributed --set fullnameOverride=loki --set ingester.persistence.enabled=true --set ingester.persistence.claims[0].name=data --set ingester.persistence.claims[0].size=200Mi
helm upgrade --install                    -n logging promtail grafana/promtail         --set config.clients[0].url=http://loki-gateway/loki/api/v1/push
