#!/usr/bin/env bash
helm repo add elastic https://helm.elastic.co
helm upgrade --install --create-namespace -n logging elasticsearch elastic/elasticsearch --set resources.requests.memory=1Gi --set resources.limits.memory=1Gi --set replicas=1 --set minimumMasterNodes=1 --set volumeClaimTemplate.resources.requests.storage=1Gi --set clusterHealthCheckParams='wait_for_status=yellow&timeout=1s'
kubectl -n logging wait --for=condition=ready pod -l app=elasticsearch-master --timeout=120s
helm upgrade --install                    -n logging filebeat      elastic/filebeat
kubectl -n logging wait --for=condition=ready pod -l app=filebeat-filebeat    --timeout=120s
helm upgrade --install                    -n logging kibana        elastic/kibana --set resources.requests.memory=1Gi --set resources.limits.memory=1Gi
kubectl -n logging patch service kibana-kibana --type merge -p "$(cat <<EOF
spec:
  type: LoadBalancer
  ports:
  - name: http
    port: 80
EOF
)"
kubectl -n logging get secrets elasticsearch-master-credentials -o jsonpath='{.data.password}' | base64 -d  # Kibana elastic password
