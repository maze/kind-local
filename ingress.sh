#!/usr/bin/env bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm upgrade --install --create-namespace -n ingress ingress ingress-nginx/ingress-nginx --set controller.ingressClassResource.default=true  #--set controller.extraArgs.enable-ssl-passthrough=true  # addition value `extraArgs` only required for vcluster.
