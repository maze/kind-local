#!/usr/bin/env bash
helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
helm upgrade --install -n kube-system metrics-server metrics-server/metrics-server --set containerPort=4443 --set args[0]="--kubelet-insecure-tls"
kubectl -n kube-system wait --for=condition=Ready pod -l app.kubernetes.io/name=metrics-server --timeout=120s
