# Local _Kubernetes in Docker_ (KinD) based lab setup
This repository contains the configuration file for provisioning a local [_KinD_](https://kind.sigs.k8s.io/) cluster and additional helper scripts for adding various add-ons to the cluster.

## Usage
- One can start the cluster with `./start.sh`.
- Commands for additional components can be found in their respective `*.sh` files.
- To destroy the cluster `./cleanup.sh`.
